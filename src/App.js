import React from "react";
import "./App.css";
import EarningsSelector from "./components/EarningsSelector";

function App() {
  return (
    <div className="App">
      <EarningsSelector />
    </div>
  );
}

export default App;
