import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { COLOR_MAP } from "./const";

function Calculation({ summ, ndfl, percent, currency }) {
  const percentToSumm = ndfl
    ? Math.round((summ / 100) * percent)
    : Math.round(summ / ((100 - percent) / 100)) - summ;
  const toHand = !ndfl ? summ : summ - percentToSumm;
  const perEmployee = !ndfl ? summ + percentToSumm : summ;
  return (
    <Container>
      <div>
        <b>
          {toHand}&nbsp;
          {currency}
        </b>
        <Description>сотрудник будет получать на руки</Description>
      </div>
      <div>
        <b>
          {percentToSumm}&nbsp;
          {currency}
        </b>
        <Description>НДФЛ, {percent}% от оклада</Description>
      </div>
      <div>
        <b>
          {perEmployee}&nbsp;
          {currency}
        </b>
        <Description>за сотрудника в месяц</Description>
      </div>
    </Container>
  );
}

const Container = styled.div`
  padding: 1rem;
  width: 20rem;
  background-color: ${COLOR_MAP.calcBg};
  color: ${COLOR_MAP.calcText};
  div :nth-child(n + 2) {
    margin-top: 1rem;
  }
`;

const Description = styled.span`
  margin-left: 0.5rem;
`;

Calculation.propTypes = {
  summ: PropTypes.number.isRequired,
  ndfl: PropTypes.bool.isRequired,
  percent: PropTypes.number.isRequired,
  currency: PropTypes.string.isRequired
};

Calculation.defaultProps = {
  percent: 13
};

export default Calculation;
