import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInfoCircle, faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import { COLOR_MAP } from "./const";

function Info({ description }) {
  const [fixed, setFixed] = React.useState(false);
  const [mouseover, setMouseover] = React.useState(false);

  const handleFixed = () => {
    setFixed(!fixed);
  };

  const handleMouseover = () => {
    setMouseover(true);
  };

  const handleMouseout = () => {
    setMouseover(false);
  };

  const showDescription = fixed || mouseover;

  return (
    <Container>
      {fixed ? (
        <FontAwesomeIconGrey
          colormap={COLOR_MAP}
          icon={faTimesCircle}
          onClick={handleFixed}
        />
      ) : (
        <FontAwesomeIconGrey
          colormap={COLOR_MAP}
          icon={faInfoCircle}
          onClick={handleFixed}
          onMouseOver={handleMouseover}
          onMouseOut={handleMouseout}
        />
      )}
      {showDescription && (
        <Tip colorMap={COLOR_MAP}>
          <p>{description}</p>
        </Tip>
      )}
    </Container>
  );
}

const FontAwesomeIconGrey = styled(FontAwesomeIcon)`
  color: ${props => props.colormap.slaveText};
  :hover {
    color: ${props => props.colormap.infoBg};
    cursor: pointer;
  }
`;

const Container = styled.div`
  margin-left: 1rem;
`;

const Tip = styled.div`
  position: absolute;
  margin-top: 1rem;
  color: white;
  z-index: 100;

  p {
    text-align: left;
    background-color: ${props => props.colormap.infoBg};
    padding: 0.8rem;
    width: 10rem;
    position: relative;
    right: -0.5rem;
    top: -0.5rem;
    color: ${props => props.colormap.infoText};
    font-size: 0.7rem;
    line-height: 1.4;
  }

  p:before {
    position: absolute;
    content: "";
    width: 0;
    height: 0;
    border-bottom: 1rem solid ${props => props.colormap.infoBg};
    border-right: 1rem solid transparent;
    border-bottom-color: ${props => props.colormap.infoBg};
    left: 0;
    top: -0.7rem;
  }
`;

Info.propTypes = {
  description: PropTypes.string.isRequired
};

Info.defaultProps = {
  description: "Не задана подсказка"
};

export default Info;
