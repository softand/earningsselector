import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const TRUE_DESCRIPTION = "Без НДФЛ";
const FALSE_DESCRIPTION = "Указать с НДФЛ";

function IosCheckbox({ defColor, checkColor, onClick, checked = false }) {
  const styledProps = {
    defColor,
    checkColor
  };
  const [stateChecked, setStateChecked] = React.useState(checked);
  const handleClick = e => {
    setStateChecked(e.target.checked);
    onClick && onClick(e.target.checked);
  };
  return (
    <Container>
      <Label checked={!stateChecked}>{FALSE_DESCRIPTION}</Label>
      <StyledCheckbox
        type="checkbox"
        {...styledProps}
        onClick={handleClick}
        defaultChecked={stateChecked}
      />
      <Label checked={stateChecked}>{TRUE_DESCRIPTION}</Label>
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  align-items: center;
`;

const StyledCheckbox = styled.input`
  margin-right: 0.5rem;
  position: relative;
  width: 1.8rem;
  height: 1rem;
  -webkit-appearance: none;
  outline: none;
  background: ${props => props.defColor};
  border-radius: 1rem;
  transition: 0.5s;
  box-shadow: inset 0 0 5px rgba(0, 0, 0, 0.2);
  :before {
    content: "";
    position: absolute;
    width: 0.9rem;
    height: 0.9rem;
    border-radius: 50%;
    top: 0.05rem;
    left: 0.05rem;
    background: white;
    transition: 0.5s;
    transform: scale(1.1);
  }
  :checked::before {
    left: 0.8rem;
  }
  :checked {
    background: ${props => props.checkColor};
  }
`;

const Label = styled.label`
  margin: 0;
  margin-right: 0.8rem;
  font-size: 0.8em;
  width: 5.1rem;
  transition: 0.5s;
  transition-props: color;
  ${props => props.checked && "color:black; font-weight:bold;"}
`;

IosCheckbox.propTypes = {
  checked: PropTypes.bool.isRequired
};

IosCheckbox.defaultProps = {
  defColor: "lightGray",
  checkColor: "orange"
};

export default IosCheckbox;
