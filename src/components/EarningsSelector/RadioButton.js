import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

function RadioButton({ title, value, name, onClick, checked, adding }) {
  const handleLabelClick = () => {
    inputRef.current.checked = true;
    onClick && onClick(value);
  };
  const handleInputClick = event => {
    const { value } = event.target;
    onClick && onClick(value);
  };
  const inputRef = React.useRef(null);
  return (
    <Container>
      <InputRadio
        ref={inputRef}
        type="radio"
        name={name}
        value={value}
        onClick={handleInputClick}
        defaultChecked={checked}
      />
      <Label onClick={handleLabelClick}>{title}</Label>
      {adding}
    </Container>
  );
}

const InputRadio = styled.input`
  position: relative;
  height: 1.5rem;
  width: 1.5rem;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  outline: none;
  ::before {
    content: "";
    position: absolute;
    top: 50%;
    left: 50%;
    width: 0.8rem;
    height: 0.8rem;
    border-radius: 50%;
    transform: translate(-50%, -50%);
    background-color: white;
    border: 1px solid grey;
  }
  :checked::after {
    content: "";
    position: absolute;
    top: 50%;
    left: 50%;
    width: 0.4rem;
    height: 0.4rem;
    border-radius: 50%;
    background-color: black;
    transform: translate(-50%, -50%);
    visibility: visible;
  }
`;

const Container = styled.div`
  display: flex;
  align-items: center;
`;

const Label = styled.label`
  margin: 0;
  font-weight: bold;
`;

RadioButton.propTypes = {
  title: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  checked: PropTypes.bool.isRequired,
  adding: PropTypes.element
};

RadioButton.defaultProps = {
  adding: null
};

export default RadioButton;
