import React from "react";
import Info from "./Info";

const EARNINGSLIST = [
  {
    title: "Оклад за месяц",
    showCalc: true,
    showEaringsInput: true,
    summ: 0,
    checked: true
  },
  {
    title: "МРОТ",
    adding: (
      <Info description="МРОТ-минимальный размер оплаты труда. Разный для разных регионов." />
    )
  },
  {
    title: "Оплата за день",
    showEaringsInput: true,
    summ: 0,
    comment: " в день"
  },
  { title: "Оплата за час", showEaringsInput: true, summ: 0, comment: " в час" }
];

const CURRENSYSYMBOL = String.fromCharCode(8381);

const COLOR_MAP = {
  infoBg: "#8b00ff",
  infoText: "#fff",
  calcBg: "#f5d08c",
  calcText: "black",
  mainText: "black",
  slaveText: "lightGray"
};

export { EARNINGSLIST, CURRENSYSYMBOL, COLOR_MAP };
