import React from "react";
import styled from "styled-components";
import IosCheckbox from "./IosCheckbox";
import RadioButton from "./RadioButton";
import Calculation from "./Calculation";
import { EARNINGSLIST, CURRENSYSYMBOL } from "./const";
import { COLOR_MAP } from "./const";

function EarningsSelector() {
  const [selectEarnings, setSelectEarings] = React.useState(EARNINGSLIST[0]);
  const [summ, setSumm] = React.useState(EARNINGSLIST[0].summ);
  const [ndfl, setHdfl] = React.useState(false);

  const setEarnings = earnings => {
    setSelectEarings(earnings);
    setSumm(earnings.summ);
  };

  const handleClick = value => {
    const earnings = EARNINGSLIST[value];
    setEarnings(earnings);
    EARNINGSLIST.forEach((earnings, id) => (earnings.checked = value == id));
  };

  const handleInputChange = event => {
    const { value } = event.target;
    selectEarnings.summ = +value;
    setSumm(+value);
  };

  const { showCalc, showEaringsInput, comment = "" } = selectEarnings;

  React.useEffect(() => {
    const findCheckedEarnings = EARNINGSLIST.find(earnings => earnings.checked);
    if (findCheckedEarnings) {
      setEarnings(findCheckedEarnings);
    }
  }, []);

  return (
    <Container>
      <Row>Сумма</Row>
      {EARNINGSLIST.map((earning, id) => {
        const { title, adding, checked } = earning;
        return (
          <RadioButton
            key={id}
            name="earnings"
            value={id}
            onClick={handleClick}
            title={title}
            adding={adding}
            checked={checked || false}
          />
        );
      })}
      {showEaringsInput && (
        <>
          <Row>
            <Tab>
              <IosCheckbox
                checked={!ndfl}
                onClick={checked => setHdfl(!checked)}
              />
            </Tab>
          </Row>
          <Row>
            <Tab>
              <EarningsInput
                type="number"
                min="0"
                max="1000000"
                value={summ}
                onChange={handleInputChange}
              />
              <Currency>{CURRENSYSYMBOL + comment}</Currency>
            </Tab>
          </Row>
        </>
      )}
      {showCalc && (
        <Row>
          <Calculation summ={summ} ndfl={ndfl} currency={CURRENSYSYMBOL} />
        </Row>
      )}
    </Container>
  );
}

const Container = styled.div``;

const Row = styled.div`
  color: ${COLOR_MAP.slaveText};
  font-size: 0.8rem;
  margin-top: 0.8rem;
`;

const Tab = styled.div`
  margin-left: 2rem;
`;

const EarningsInput = styled.input`
  outline: none !important;
  border-radius: 0.5rem;
  border: 1px solid ${COLOR_MAP.slaveText};
  width: 10rem;
  font-weight: bold;
  ::-webkit-outer-spin-button,
  ::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
  }
`;

const Currency = styled.span`
  margin-left: 0.2rem;
  font-weight: bold;
  color: ${COLOR_MAP.mainText};
`;

export default EarningsSelector;
